package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    //Field variables
    int colF = 0;
    int rowF = 0;

    //CellState datastructure
    CellState[][] grid = new CellState [colF][rowF];


    public CellGrid(int rows, int columns, CellState initialState) {
		
        this.rowF = rows;
        this.colF = columns;

        grid = new CellState[rows] [columns];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                grid[row] [col] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return rowF;
    }

    @Override
    public int numColumns() {
        return colF;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row >= numRows() || column < 0 || column >= numColumns()) {
            throw new IndexOutOfBoundsException();
        } else {
            grid[row] [column] = element;
        }
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || row >= numRows() || column < 0 || column >= numColumns()) {
            throw new IndexOutOfBoundsException();
        } else {
        return grid[row][column];
        }
    }

    @Override
    public IGrid copy() {
        CellGrid gridCopy = new CellGrid(this.numRows(), this.numColumns(), CellState.DEAD);
        gridCopy.grid = this.grid;
        return gridCopy;
    }
    
}
