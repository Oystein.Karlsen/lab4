package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;


public class BriansBrain implements CellAutomaton {

    int row = 0;
	int col = 0;

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
		row = rows;
		col = columns;
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return row;
	}

	@Override
	public int numberOfColumns() {
		return col;
	}

	@Override
	public CellState getCellState(int row, int col) {
		CellState state = currentGeneration.get(row, col);
		return state;
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
	}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState nextState = currentGeneration.get(row, col);

		//Alive to dying
		if (getCellState(row, col) == CellState.ALIVE){
			nextState = CellState.DYING;
		} 
        //Dying to dead
        if (getCellState(row, col) == CellState.DYING){
			nextState = CellState.DEAD;
		} 
		//3 Neighbors, do Jesus
		if (getCellState(row, col) == CellState.DEAD && countNeighbors(row, col, CellState.ALIVE) == 2) {
			nextState = CellState.ALIVE;
		}


		return nextState;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int neighbors = 0;
        for (int i : new int[]{-1, 0, 1}) {
            for (int j : new int[]{-1, 0, 1}) {
                if (i == 0 && j == 0) {
                    continue;
                }
                CellState currentState = currentGeneration.get(
                        Math.floorMod(row +i, numberOfRows())
                        , Math.floorMod(col + j, numberOfColumns()));
                if (currentState.equals(state)) {
                    neighbors++;
                }
            }
        }
        return neighbors;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
